package oak.forge.commons.rabbitmq.topic

import com.rabbitmq.client.Channel
import oak.forge.commons.rabbitmq.topic.factory.EventReceiverConfig
import oak.forge.commons.rabbitmq.topic.factory.TopicData
import spock.lang.Shared

class EventReceiverConfigTest extends RabbitMqContainerConfig{

    @Shared
    def eventReceiverConfig

    @Shared
    Channel channel

    void setupSpec() {

        container.start()
        channel = createChannel()
        eventReceiverConfig = new EventReceiverConfig()
    }

    def "should get publish exchange"() {

        given:
        String routingKey = "routingKey"

        when:
        def exchange = eventReceiverConfig.getPublishExchange(routingKey)

        then:
        exchange == TopicData.SECURED_INCOMING_EXCHANGE
    }

    def "should get queses to subscribe"() {

        given:
        def clientId = UUID.randomUUID()

        when:
        def quesesToSubscribe = eventReceiverConfig.getQueuesToSubscribe(clientId)

        then:
        quesesToSubscribe.size() == 1

        def eventsQueue = quesesToSubscribe.get(0)
        eventsQueue == TopicData.EVENTS_QUEUE
    }

    def "should declare queues"(){

        given:
        def clientId = UUID.randomUUID()
        String message = "message"

        when:
        eventReceiverConfig.declareQueuesConfig(channel, clientId)
        channel.basicPublish(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.EVENTS_ROUTING_KEY, null, message.getBytes())

        then:
        def messageFromQueue = channel.basicGet(TopicData.EVENTS_QUEUE, true)
        messageFromQueue.getBody() == message.getBytes()
        messageFromQueue.getMessageCount() == 0
    }
}