package oak.forge.commons.rabbitmq.topic

import com.rabbitmq.client.Channel
import com.rabbitmq.client.ConnectionFactory
import org.testcontainers.containers.RabbitMQContainer
import spock.lang.Shared
import spock.lang.Specification

class RabbitMqContainerConfig extends Specification {

    @Shared
    RabbitMQContainer container = new RabbitMQContainer("rabbitmq:3.8-management")
            .withUser("testUser", "testUser")
            .withPermission("/", "testUser", ".*", ".*", ".*")
            .withExposedPorts(5672, 15672)
            .withReuse(true)

    Channel createChannel(){

        ConnectionFactory connectionFactory = new ConnectionFactory()
        connectionFactory.setUsername("testUser")
        connectionFactory.setPassword("testUser")
        connectionFactory.setPort(container.getAmqpPort())
        connectionFactory.setHost(container.getHost())
        def connection = connectionFactory.newConnection()
        def channel = connection.createChannel()
        return channel
    }
}