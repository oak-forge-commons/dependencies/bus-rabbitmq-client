package oak.forge.commons.rabbitmq.topic

import oak.forge.commons.rabbitmq.utils.TestMessage
import oak.forge.commons.rabbitmq.topic.factory.ExternalConfig
import oak.forge.commons.rabbitmq.topic.factory.TopicData
import spock.lang.Shared

class ExternalConfigTest extends RabbitMqContainerConfig {

    @Shared
    def externalConfig

    void setupSpec() {

        container.start()
        externalConfig = new ExternalConfig()
    }

    def "should return queues to subscribe"() {

        given:
        UUID clientId = UUID.randomUUID()

        when:
        def quesesToSubscribe = externalConfig.getQueuesToSubscribe(clientId)

        then:
        def getQueue = quesesToSubscribe.get(0)
        getQueue == externalConfig.configName + "-" + clientId
    }

    def "should return default publish exchange"() {

        given:
        String routingKey = "routingKey"

        when:
        def exchange = externalConfig.getPublishExchange(routingKey)

        then:
        exchange == TopicData.UNSECURED_EXCHANGE
    }

    def "should set a clientId as a message sender"() {

        given:
        TestMessage message = new TestMessage()
        UUID clientId = UUID.randomUUID()

        when:
        externalConfig.handleSender(message, clientId)

        then:
        message.senderId == clientId
    }

    def "should declare queues"() {

        given:
        def channel = createChannel()
        def clientId = UUID.randomUUID()

        String publicMessage = "message1"
        String privateMessage = "message2"

        when:
        externalConfig.declareQueuesConfig(channel,clientId)
        channel.basicPublish(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.PUBLIC_ROUTING_KEY, null, publicMessage.getBytes())
        channel.basicPublish(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.PRIVATE_ROUTING_KEY + clientId,null, privateMessage.getBytes())

        then:
        def publicMessageForClient = channel.basicGet("client-" + clientId, false)
        publicMessageForClient.getBody() == publicMessage.getBytes()
        publicMessageForClient.getMessageCount() == 1

        def privateMessageForClient = channel.basicGet("client-" + clientId, false)
        privateMessageForClient.getBody() == privateMessage.getBytes()
        privateMessageForClient.getMessageCount() == 0
    }
}













