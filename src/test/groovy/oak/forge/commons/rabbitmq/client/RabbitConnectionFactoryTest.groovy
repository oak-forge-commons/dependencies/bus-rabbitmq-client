package oak.forge.commons.rabbitmq.client

import oak.forge.commons.rabbitmq.connection.RabbitConfigFactory
import oak.forge.commons.rabbitmq.connection.RabbitConnectionData
import oak.forge.commons.rabbitmq.connection.RabbitConnectionFactory
import oak.forge.commons.rabbitmq.exception.RabbitClientConnectionException
import oak.forge.commons.rabbitmq.exception.RabbitClientConsumerException
import oak.forge.commons.rabbitmq.topic.RabbitMqContainerConfig
import oak.forge.commons.rabbitmq.topic.TopicFactory
import oak.forge.commons.rabbitmq.topic.factory.ExternalConfig
import spock.lang.Shared

class RabbitConnectionFactoryTest extends RabbitMqContainerConfig {
    @Shared
    RabbitConnectionFactory rabbitConnectionFactory
    @Shared
    RabbitConfigFactory rabbitConfigFactory
    @Shared
    RabbitConnectionData rabbitConnectionData
    @Shared
    TopicFactory topicFactory

    void setupSpec() {
        container.start()
        rabbitConnectionData = Mock()
        rabbitConfigFactory = new RabbitConfigFactory(rabbitConnectionData, null)
        rabbitConnectionFactory = new RabbitConnectionFactory(rabbitConfigFactory)
        topicFactory = new ExternalConfig()
    }

    def "should return exception when connection is not initialized"(){
        given:
        UUID clientId = UUID.randomUUID()

        when:
        rabbitConnectionFactory.initializeConfiguredConnection(topicFactory, null, clientId)

        then:
        def exception = thrown(RabbitClientConnectionException.class)
        exception.getMessage().startsWith("Create connection occur error. ") == true
    }

    def "should return exception when can't consuming from queue"() {
        given:
        def channel = createChannel()

        when:
        rabbitConnectionFactory.declareSubscription(topicFactory, channel, null, null)

        then:
        def exception = thrown(RabbitClientConsumerException.class)
        exception.getMessage().startsWith("Error when consuming from queue: ") == true
    }
}