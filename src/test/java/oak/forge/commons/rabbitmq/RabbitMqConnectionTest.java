package oak.forge.commons.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.rabbitmq.client.*;
import oak.forge.commons.rabbitmq.connection.*;
import oak.forge.commons.rabbitmq.exception.RabbitClientConnectionException;
import oak.forge.commons.rabbitmq.mapper.DefaultObjectMapper;
import oak.forge.commons.rabbitmq.topic.factory.*;
import oak.forge.commons.rabbitmq.utils.TestCommand;
import oak.forge.commons.rabbitmq.utils.TestEvent;
import oak.forge.commons.rabbitmq.utils.TestGenericError;
import org.junit.jupiter.api.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.testcontainers.containers.RabbitMQContainer;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

import static org.testcontainers.utility.MountableFile.forClasspathResource;

@SpringJUnitConfig
public class RabbitMqConnectionTest {

    @Configuration
    public static class Config {

        @Bean
        RabbitConnectionData rabbitConnectionConfig() {
            return new RabbitConnectionData("guest", "adminPassword", container.getHost(), container.getAmqpsPort(), true);
        }

        @Bean
        SslConfig sslConfig(){
            return new SslConfig("bunnies",
                    "rabbitmqCerts/client_key.p12","rabbitmqCerts/server_store.jks");
        }

        @Bean
        RabbitConfigFactory configFactory(RabbitConnectionData connectionConfig,SslConfig sslConfig) {
            return new RabbitConfigFactory(connectionConfig, sslConfig);
        }

        @Bean
        ObjectMapper objectMapper() {
            return new DefaultObjectMapper();
        }

        @Bean
        RabbitConnectionFactory connectionConfigFactory(RabbitConfigFactory rabbitConfigFactory){
            return new RabbitConnectionFactory(rabbitConfigFactory);
        }

        @Bean
        RabbitClientFactory clientFactory(RabbitConnectionFactory configFactory, ObjectMapper objectMapper){
            return new RabbitClientFactory(configFactory, objectMapper);
        }

        @Bean
        RabbitBusClient extClient1(RabbitClientFactory rabbitClientFactory) throws RabbitClientConnectionException {
            return rabbitClientFactory.prepareBusClient(ConfigType.EXTERNAL);
        }

        @Bean
        RabbitBusClient extClient2(RabbitClientFactory rabbitClientFactory) throws RabbitClientConnectionException {
            return rabbitClientFactory.prepareBusClient(ConfigType.EXTERNAL);
        }

        @Bean
        RabbitBusClient filteringService(RabbitClientFactory rabbitClientFactory) throws RabbitClientConnectionException {
            return rabbitClientFactory.prepareBusClient(ConfigType.FILTERING);
        }

        @Bean
        RabbitBusClient aggregateBusClient(RabbitClientFactory rabbitClientFactory) throws RabbitClientConnectionException {
            return rabbitClientFactory.prepareBusClient(ConfigType.AGGREGATE);
        }

        @Bean
        RabbitBusClient eventReceiver(RabbitClientFactory rabbitClientFactory) throws RabbitClientConnectionException {
            return rabbitClientFactory.prepareBusClient(ConfigType.EVENT_RECEIVER);
        }
    }

    Logger logger = LoggerFactory.getLogger(RabbitMqConnectionTest.class);

    public static final RabbitMQContainer container = new RabbitMQContainer("rabbitmq:3.8-management")
            .withAdminPassword("adminPassword")
            .withExposedPorts(5671, 15671)
            .withSSL(forClasspathResource("/rabbitmqCerts/server_key.pem"),
                    forClasspathResource("/rabbitmqCerts/server_certificate.pem"),
                    forClasspathResource("/rabbitmqCerts/ca_certificate.pem"),
                    RabbitMQContainer.SslVerification.VERIFY_PEER,
                    true);


    @Autowired
    RabbitBusClient extClient1;

    @Autowired
    RabbitBusClient extClient2;

    @Autowired
    RabbitBusClient filteringService;

    @Autowired
    RabbitBusClient aggregateBusClient;

    @Autowired
    RabbitBusClient eventReceiver;

    @Autowired
    RabbitConfigFactory configFactory;

    @BeforeAll
    public static void setup() {
        container.start();
    }

    @BeforeEach
    void setupForEachTest() {
        extClient1.unregisterMessageHandlers();
        extClient2.unregisterMessageHandlers();
        filteringService.unregisterMessageHandlers();
        aggregateBusClient.unregisterMessageHandlers();
    }

    @Test
    void check_if_rabbitContainer_is_running() {
        assertThat(container.isRunning()).isTrue();
    }

    @Test
    void should_post_message_without_exception() {
        // given
        TestCommand testMessage = new TestCommand("name");
        // then
        assertThatCode(() -> extClient1.post(testMessage)).doesNotThrowAnyException();
    }

    @Test
    void should_message_be_properly_filtered() throws InterruptedException {
        // given
        CountDownLatch countDownLatch = new CountDownLatch(2);

        TestCommand command = new TestCommand("command");
        Consumer<TestCommand> filtering = mock(Consumer.class);
        Consumer<TestCommand> aggregate = mock(Consumer.class);

        filteringService.registerMessageHandler(TestCommand.class, message -> {
            filtering.accept(message);
            countDownLatch.countDown();
            filteringService.post(message);
        });

        aggregateBusClient.registerMessageHandler(TestCommand.class, testCommand -> {
            aggregate.accept(testCommand);
            countDownLatch.countDown();
            TestEvent testEvent = new TestEvent("event");
            aggregateBusClient.post(testEvent);
        });

        // when
        extClient1.post(command);

        // then
        countDownLatch.await();
        then(filtering).should().accept(refEq(command));
        then(aggregate).should().accept(refEq(command));
    }

    @Test
    void should_produce_event_to_all_clients_if_message_is_public() throws InterruptedException {
        //given..
        CountDownLatch countDownLatch = new CountDownLatch(4);

        TestCommand command = new TestCommand("command");
        Consumer<TestEvent> client1 = mock(Consumer.class);
        Consumer<TestEvent> client2 = mock(Consumer.class);
        Consumer<Command> filtering = mock(Consumer.class);
        Consumer<TestCommand> aggregate = mock(Consumer.class);
        TestEvent publicEvent = new TestEvent("event");

        filteringService.registerMessageHandler(Command.class, message -> {
            filtering.accept(message);
            countDownLatch.countDown();
            filteringService.post(message);
        });

        aggregateBusClient.registerMessageHandler(TestCommand.class, message -> {
            aggregate.accept(message);
            countDownLatch.countDown();
            aggregateBusClient.post(publicEvent);
        });

        extClient1.registerMessageHandler(TestEvent.class, message -> {
            client1.accept(message);
            countDownLatch.countDown();
        });

        extClient2.registerMessageHandler(TestEvent.class, message -> {
            client2.accept(message);
            countDownLatch.countDown();
        });

        //when..
        extClient1.post(command);

        //then..
        countDownLatch.await();
        then(client1).should().accept(refEq(publicEvent));
        then(client2).should().accept(refEq(publicEvent));

    }

    @Test
    void should_produce_event_to_client_with_correct_id_if_message_is_private() throws InterruptedException {
        //given..
        CountDownLatch countDownLatch = new CountDownLatch(3);

        TestCommand command = new TestCommand("command");

        Consumer<Integer> client1 = mock(Consumer.class);
        Consumer<Integer> client2 = mock(Consumer.class);
        Consumer<TestCommand> filtering = mock(Consumer.class);
        Consumer<TestCommand> aggregate = mock(Consumer.class);

        filteringService.registerMessageHandler(TestCommand.class, message -> {
            filtering.accept(message);
            countDownLatch.countDown();
            filteringService.post(message);
        });

        aggregateBusClient.registerMessageHandler(TestCommand.class, message -> {
            aggregate.accept(message);
            countDownLatch.countDown();
            TestEvent testEventMessage = new TestEvent(message.getSenderId(), "event");
            testEventMessage.setValue(17);
            aggregateBusClient.post(testEventMessage);
        });

        extClient1.registerMessageHandler(TestEvent.class, message -> {
            client1.accept(message.getValue());
            countDownLatch.countDown();
        });

        extClient2.registerMessageHandler(TestEvent.class, message -> {
            client2.accept(message.getValue());
            countDownLatch.countDown();
        });

        //when..
        extClient1.post(command);

        //then..
        countDownLatch.await();
        then(client1).should().accept(17);
        then(client2).shouldHaveNoInteractions();
    }

    @Test
    void should_get_produced_event_as_event_receiver() throws InterruptedException {
        //given
        CountDownLatch countDownLatch = new CountDownLatch(3);

        TestCommand command = new TestCommand("command");

        Consumer<String> eventListener1 = mock(Consumer.class);

        filteringService.registerMessageHandler(TestCommand.class, message -> {
            filteringService.post(message);
            countDownLatch.countDown();
        });

        aggregateBusClient.registerMessageHandler(TestCommand.class, message -> {
            TestEvent testEvent = new TestEvent(message.getSenderId(), "event");
            aggregateBusClient.post(testEvent);
            countDownLatch.countDown();
        });

        eventReceiver.registerMessageHandler(TestEvent.class, event -> {
            eventListener1.accept(event.getName());
            countDownLatch.countDown();
        });

        //when..
        extClient1.post(command);

        //then..
        countDownLatch.await();
        then(eventListener1).should().accept("event");

    }

    @Test
    void should_produce_generic_error_to_client_with_correct_id() throws InterruptedException {
        //given..
        CountDownLatch countDownLatch = new CountDownLatch(2);

        TestCommand command = new TestCommand("command");

        Consumer<UUID> client1 = mock(Consumer.class);
        Consumer<UUID> client2 = mock(Consumer.class);
        Consumer<TestCommand> filtering = mock(Consumer.class);
        Consumer<TestCommand> aggregate = mock(Consumer.class);

        filteringService.registerMessageHandler(TestCommand.class, message -> {
            filtering.accept(message);
            countDownLatch.countDown();
            TestGenericError genericError = new TestGenericError(message.getSenderId(), "error");
            filteringService.post(genericError);
        });

        aggregateBusClient.registerMessageHandler(TestCommand.class, message -> {
            aggregate.accept(message);
            countDownLatch.countDown();
        });

        extClient1.registerMessageHandler(TestGenericError.class, message -> {
            client1.accept(message.getReceiverId());
            countDownLatch.countDown();
        });

        extClient2.registerMessageHandler(TestGenericError.class, message -> {
            client2.accept(message.getReceiverId());
            countDownLatch.countDown();
        });

        //when..
        extClient1.post(command);

        //then..
        countDownLatch.await();
        then(aggregate).shouldHaveNoInteractions();
        then(client1).should().accept(command.getSenderId());
        then(client2).shouldHaveNoInteractions();
    }

    @Test
    void should_open_connection_with_given_name_using_connection_factory() throws IOException, TimeoutException {
        //given
        ConnectionFactory connectionFactory = configFactory.getConnectionFactory();
        String connectionName = "TestConnection";

        //when
        Connection connection = connectionFactory.newConnection(connectionName);

        //then
        assertThat(connection.isOpen()).isTrue();
        assertThat(connection.getClientProvidedName()).isEqualTo(connectionName);
    }


}
