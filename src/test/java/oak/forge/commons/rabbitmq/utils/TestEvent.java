package oak.forge.commons.rabbitmq.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;
import oak.forge.commons.data.message.Event;

import java.util.UUID;

public class TestEvent extends Event {

    private String name;
    private int value;

    @JsonCreator
    public TestEvent(String name) {
        super();
        this.name = name;
    }

    public TestEvent(UUID receiverId, String name) {
        super(receiverId);
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
