package oak.forge.commons.rabbitmq.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import oak.forge.commons.data.message.GenericError;

import java.util.UUID;

@Getter
public class TestGenericError extends GenericError {
    private String name;

    @JsonCreator
    public TestGenericError(String name) {
        super();
        this.name = name;
    }

    public TestGenericError(UUID receiverId, String name) {
        super(receiverId);
        this.name = name;
    }
}
