package oak.forge.commons.rabbitmq.exception;

public class RabbitClientConnectionException extends RuntimeException {

    public RabbitClientConnectionException(String message) {
        super(message);
    }
}
