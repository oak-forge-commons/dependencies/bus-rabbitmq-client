package oak.forge.commons.rabbitmq.exception;

public class RabbitClientConsumerException extends RuntimeException{

    public RabbitClientConsumerException(String message) {
        super(message);
    }
}
