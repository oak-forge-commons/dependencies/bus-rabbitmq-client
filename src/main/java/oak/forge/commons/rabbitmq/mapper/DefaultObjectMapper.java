package oak.forge.commons.rabbitmq.mapper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paranamer.ParanamerOnJacksonAnnotationIntrospector;


public class DefaultObjectMapper extends ObjectMapper {

    public DefaultObjectMapper() {
        super();
        setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.ANY);
        registerModule(new JavaTimeModule());
        setAnnotationIntrospector(new ParanamerOnJacksonAnnotationIntrospector());
    }
}
