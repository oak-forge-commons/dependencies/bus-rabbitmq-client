package oak.forge.commons.rabbitmq.connection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RabbitConnectionData {

    private String username;
    private String password;
    private String host;
    private int port;
    private boolean sslOn;

    public RabbitConnectionData(String username, String password, String host, int port, boolean sslOn) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.sslOn = sslOn;
    }
}
