package oak.forge.commons.rabbitmq.topic.factory;

import com.rabbitmq.client.Channel;
import oak.forge.commons.data.message.Message;
import oak.forge.commons.rabbitmq.topic.*;

import java.io.IOException;
import java.util.*;

public class EventReceiverConfig extends TopicFactory {

    private final static String DEFAULT_PUBLISH_EXCHANGE = TopicData.SECURED_INCOMING_EXCHANGE;

    public EventReceiverConfig() {
        super("eventReceiver");
    }

    @Override
    public void declareQueuesConfig(Channel channel, UUID clientId) throws IOException {
        channel.exchangeDeclare(DEFAULT_PUBLISH_EXCHANGE, TopicData.TYPE_TOPIC);
        channel.exchangeDeclare(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.TYPE_TOPIC);

        queueDeclare(channel, TopicData.EVENTS_QUEUE);

        queueBind(channel, TopicData.EVENTS_QUEUE, TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.EVENTS_ROUTING_KEY);

    }

    @Override
    public String getPublishExchange(String routingKey) {
        return DEFAULT_PUBLISH_EXCHANGE;
    }

    @Override
    public List<String> getQueuesToSubscribe(UUID clientId) {
        List<String> queuesToSubscribe = new ArrayList<>();
        queuesToSubscribe.add(TopicData.EVENTS_QUEUE);
        return queuesToSubscribe;
    }

    @Override
    public <M extends Message> void handleSender(M message, UUID clientId) {

    }

}
