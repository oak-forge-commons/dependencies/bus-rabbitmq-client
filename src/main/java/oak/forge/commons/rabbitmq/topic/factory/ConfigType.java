package oak.forge.commons.rabbitmq.topic.factory;

import lombok.*;
import oak.forge.commons.rabbitmq.topic.TopicFactory;

import java.util.function.Supplier;

/**
 * Enumeration for different types of topic configuration.
 */
@RequiredArgsConstructor
@Getter
public enum ConfigType {

    AGGREGATE(AggregateConfig::new),
    EXTERNAL(ExternalConfig::new),
    FILTERING(FilteringConfig::new),
    EVENT_RECEIVER(EventReceiverConfig::new);

    private final Supplier<TopicFactory> constructor;
}
