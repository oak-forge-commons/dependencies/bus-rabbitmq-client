package oak.forge.commons.rabbitmq.topic.factory;

import com.rabbitmq.client.Channel;
import oak.forge.commons.data.message.Message;
import oak.forge.commons.rabbitmq.topic.*;

import java.io.IOException;
import java.util.*;

public class ExternalConfig extends TopicFactory {

    public ExternalConfig() {
        super("client");
    }

    @Override
    public void declareQueuesConfig(Channel channel, UUID clientId) throws IOException {
        String exclusiveQueue = getExclusiveQueue(getConfigName(), clientId);
        String privateRoutingKey = getPrivateRoutingKey(clientId);

        channel.exchangeDeclare(getPublishExchange(null), TopicData.TYPE_TOPIC);
        channel.exchangeDeclare(TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.TYPE_TOPIC);

        queueDeclareExclusive(channel, exclusiveQueue);

        queueBind(channel, exclusiveQueue, TopicData.SECURED_OUTGOING_EXCHANGE, privateRoutingKey);
        queueBind(channel, exclusiveQueue, TopicData.SECURED_OUTGOING_EXCHANGE, TopicData.PUBLIC_ROUTING_KEY);
    }

    @Override
    public String getPublishExchange(String routingKey) {
        return TopicData.UNSECURED_EXCHANGE;
    }

    @Override
    public List<String> getQueuesToSubscribe(UUID clientId) {
        List<String> queuesToSubscribe = new ArrayList<>();
        queuesToSubscribe.add(getExclusiveQueue(getConfigName(), clientId));
        return queuesToSubscribe;
    }

    @Override
    public <M extends Message> void handleSender(M message, UUID clientId) {
        message.setSenderId(clientId);
    }
}
