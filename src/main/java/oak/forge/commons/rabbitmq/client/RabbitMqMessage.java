package oak.forge.commons.rabbitmq.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
public class RabbitMqMessage {

    private String routingKey;
    private Instant timestamp;
    private String messageType;
    private String payload;

    @JsonCreator
    public RabbitMqMessage(String routingKey, String messageType, String payload) {
        this.routingKey = routingKey;
        this.messageType = messageType;
        this.payload = payload;
    }
}
