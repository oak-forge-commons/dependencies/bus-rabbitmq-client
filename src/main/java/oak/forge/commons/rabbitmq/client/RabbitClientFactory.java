package oak.forge.commons.rabbitmq.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import oak.forge.commons.rabbitmq.exception.RabbitClientConnectionException;
import oak.forge.commons.rabbitmq.connection.*;
import oak.forge.commons.rabbitmq.topic.factory.ConfigType;

public class RabbitClientFactory {

    private final RabbitConnectionFactory rabbitConnectionFactory;
    private final ObjectMapper objectMapper;

    public RabbitClientFactory(RabbitConnectionFactory rabbitConnectionFactory, ObjectMapper objectMapper) {
        this.rabbitConnectionFactory = rabbitConnectionFactory;
        this.objectMapper = objectMapper;
    }

    public RabbitBusClient prepareBusClient(ConfigType configType) throws RabbitClientConnectionException {
        return new RabbitBusClient(rabbitConnectionFactory, configType.getConstructor().get(), objectMapper);
    }


}
