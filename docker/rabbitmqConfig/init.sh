#!/bin/bash

(
  while ! nc -z rabbitmq 5671; do
    sleep 3
    echo "Waiting for port 5671"
  done
  rabbitmqctl add_user "$RABBITMQ_USER" "$RABBITMQ_PASSWORD"
  rabbitmqctl set_user_tags "$RABBITMQ_USER" administrator
  rabbitmqctl set_permissions -p / "$RABBITMQ_USER" ".*" ".*" ".*"
  echo "*** User '$RABBITMQ_USER' with password '$RABBITMQ_PASSWORD' created. ***"
  rabbitmqctl delete_user guest
  echo "*** Unused user 'guest' deleted ***"
) &
# shellcheck disable=SC2068
rabbitmq-server $@
